const {src, dest, task, series, watch, parallel} = require("gulp");
const rm = require("gulp-rm");
const sass = require("gulp-sass")(require("node-sass"));
const browserSync = require("browser-sync").create();
const autoprefixer = require("gulp-autoprefixer");
const concat = require("gulp-concat");
const sourcemaps = require("gulp-sourcemaps");
const sassGlob = require("gulp-sass-glob");
const gulpif = require("gulp-if");
const reload = browserSync.reload;
const htmlmin = require('gulp-htmlmin');
const cleanCSS = require("gulp-clean-css");
const rename = require("gulp-rename");

const {SRC_PATH, DIST_PATH} = require("./gulp.config.js");

const env = process.env.NODE_ENV;

task("clean", () => {
    return src(`${DIST_PATH}/**/*`, {read: false}).pipe(rm());
});

task("copy:images", () => {
    return src(`${SRC_PATH}/images/**/*`)
        .pipe(dest(`${DIST_PATH}/images`))
        .pipe(reload({stream: true}));
});

task("fonts", () => {
    return src(`${SRC_PATH}/fonts/**/*`)
        .pipe(
            rename(function (path) {
                path.basename = `${path.basename.toLowerCase()}`;
            })
        )
        .pipe(dest(`${DIST_PATH}/fonts`))
        .pipe(reload({stream: true}));
});

task("html", () => {
    return src(`${SRC_PATH}/*.html`)
        .pipe(gulpif(env === "dev", htmlmin({collapseWhitespace: true})))
        .pipe(dest(`${DIST_PATH}`))
        .pipe(reload({stream: true}));
});

task("styles", () => {
    return src([`${SRC_PATH}/scss/index.scss`])
        .pipe(gulpif(env === "dev", sourcemaps.init()))
        .pipe(sassGlob())
        .pipe(sass().on("error", sass.logError))
        // .pipe(gulpif(env === "prod", cleanCSS())) // minimization
        .pipe(gulpif(env === "prod", autoprefixer()))
        .pipe(gulpif(env === "dev", sourcemaps.write()))
        .pipe(concat("styles.css"))
        .pipe(dest(`${DIST_PATH}/css`))
        .pipe(reload({stream: true}));
});

task("server", () => {
    browserSync.init({
        server: {
            baseDir: `${DIST_PATH}`
        }
    })
});
task("watch", () => {
    watch(`${SRC_PATH}/images/**/**`, series("copy:images")).on("change", reload);
    watch(`${SRC_PATH}/fonts/**/**`, series("fonts")).on("change", reload);
    watch(`${SRC_PATH}/*.html`, series("html")).on("change", reload);
    watch(`${SRC_PATH}/scss/**/*.scss`, series("styles")).on("change", reload);
});
task(
    "default",
    series(
        "clean",
        parallel("copy:images", "fonts", "html", "styles"),
        parallel("watch", "server")
    )
);